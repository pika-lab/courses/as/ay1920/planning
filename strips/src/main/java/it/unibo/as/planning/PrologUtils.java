package it.unibo.as.planning;

import alice.tuprolog.*;

import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static alice.tuprolog.Terms.anonymous;
import static alice.tuprolog.Terms.struct;

public class PrologUtils {
    public static boolean assertA(Prolog engine, Term term) {
        return engine.solve(struct("asserta", term)).isSuccess();
    }

    public static boolean assertZ(Prolog engine, Term term) {
        return engine.solve(struct("assertz", term)).isSuccess();
    }

    public static boolean retract(Prolog engine, Term term) {
        return engine.solve(struct("retract", term)).isSuccess();
    }

    public static boolean retractAll(Prolog engine, Term term) {
        return engine.solve(struct("retractall", term)).isSuccess();
    }

    public static boolean update(Prolog engine, Struct term) {
        final Term[] placeholders = IntStream.range(0, term.getArity())
                .mapToObj(i -> anonymous())
                .toArray(Term[]::new);

        return retract(engine, struct(term.getName(), placeholders))
                && assertA(engine, term);
    }

}
