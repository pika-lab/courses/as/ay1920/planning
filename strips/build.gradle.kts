task<JavaExec>("run") {
    dependsOn("assemble")
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "it.unibo.as.planning.Main"
    standardInput = System.`in`

    var temp = listOf<String>()

    if (properties.containsKey("verbose") && properties["verbose"] == "true") {
        temp += listOf("--verbose")
    }

    if (properties.containsKey("initialState")) {
        temp += listOf("--initialState", "${properties["initialState"]}")
    }

    if (properties.containsKey("goal")) {
        temp += listOf("--goal", "${properties["goal"]}")
    }

    if (properties.containsKey("world")) {
        temp += listOf("--world", "${properties["world"]}")
    }

    if (properties.containsKey("maxPlanLength")) {
        temp += listOf("--maxPlanLength", "${properties["maxPlanLength"]}")
    }

    args = temp

    doFirst {
        println("Running `${main}` with arguments `${args!!.joinToString(" ")}`")
    }
}
