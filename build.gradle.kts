import de.undercouch.gradle.tasks.download.Download

plugins {
    java
    id("de.undercouch.download") version("3.4.3")
}

repositories {
    mavenCentral()
}

group = "it.unibo.as"
version = "1.0-SNAPSHOT"

subprojects {

    repositories {
        mavenCentral()
    }

    apply<JavaPlugin>()
    //apply<ApplicationPlugin>()

    dependencies {
        implementation("it.unibo.alice.tuprolog", "2p-core", "4.1.1")
        testImplementation("junit", "junit", "4.12")
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    val prolog by configurations.creating
    prolog("it.unibo.alice.tuprolog", "2p-ui", "4.1.1")
}

task<JavaExec>("2p") {
    group = "tuprolog"
    classpath = configurations.getByName("prolog")
    main = "alice.tuprologx.ide.CUIConsole"
    standardInput = System.`in`

    if (properties.containsKey("theory")) {
        args = listOf(properties["theory"].toString())
    }

    doFirst {
        if (properties.containsKey("theory")) {
            println("Loading theory: ${properties["theory"]}")
        }
    }
}

task<JavaExec>("2p-gui") {
    group = "tuprolog"
    classpath = configurations.getByName("prolog")
    main = "alice.tuprologx.ide.GUILauncher"
    standardInput = System.`in`
}